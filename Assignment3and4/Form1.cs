﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment3and4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Welcome to our Ryde app");
            string From = textBox1.Text;
            string To = textBox2.Text;
            double BaseFare = 2.5;
            double DistanceCharge = 0.81;
            double ServiceFee = 1.75;
            double totalPrice = 0.0;
            string rb;

            if (From == "")
            {
                MessageBox.Show("Please Enter your location");
            }
            else if (To == "")
            {
                MessageBox.Show("Please enter your destination");
            }
            else if (From =="Fairveiw Mall" && To=="Tim Hortons")
            {
                if (radioButton1.Checked == true)
                {
                    rb = radioButton1.Text;
                    //totalPrice = BaseFare +(DistanceCharge * Distance ) + ServiceFee;

                    totalPrice = 2.5 + (0.81 * 1.2) + 1.75;

                    MessageBox.Show(rb + "is confirmed !!" + "\n" + To + "\n" + "From: " + From + "\n" + "Base Fare : $"+
                        "\n" + "Distance Charge : $" + DistanceCharge.ToString() + "per km " + "\n" + " Service Fee : $" + ServiceFee.ToString() + "\n" +" Total Price : $"+totalPrice);
                }
                else if(radioButton2.Checked== true)
                {
                    rb = radioButton2.Text;

                    totalPrice = 2.5 + (0.81 * 1.2) + 1.75;

                    MessageBox.Show(rb + "is confirmed !!" + "\n" + To + "\n" + "From: " + From + "\n" + "Base Fare : $"+
                        "\n" + "Distance Charge : $" + DistanceCharge.ToString() + "per km " + "\n" + " Service Fee : $" + ServiceFee.ToString() + "\n" + " Total Price : $" + totalPrice);
                }
                else
                {
                    MessageBox.Show("Please select which type of ryde you want !!");
                }
            }
            else if (From == "275 Yorkland Blvd" && To == "CN Tower")
            {
                if (radioButton1.Checked == true)
                {
                    rb = radioButton1.Text;

                    totalPrice = 2.5 + (0.81 * 22.9) + 1.75;

                    MessageBox.Show(rb + "is confirmed !!" + "\n" + To + "\n" + "From: " + From + "\n" + "Base Fare : $"+
                        "\n" + "Distance Charge : $" + DistanceCharge.ToString() + "per km " + "\n" + " Service Fee : $" + ServiceFee.ToString() + "\n" + " Total Price : $" + totalPrice);
                }
                else if (radioButton2.Checked == true)
                {
                    rb = radioButton2.Text;

                    totalPrice = 2.5 + (0.81 * 22.9) + 1.75;

                    MessageBox.Show(rb + "is confirmed !!" + "\n" + To + "\n" + "From: " + From + "\n" + "Base Fare : $"+
                        "\n" + "Distance Charge : $" + DistanceCharge.ToString() + "per km " + "\n" + " Service Fee : $" + ServiceFee.ToString() + "\n" + " Total Price : $" + totalPrice);
                }
                else
                {
                    MessageBox.Show("Please select which type of ryde you want !!");
                }
            }
            else
            {
                MessageBox.Show("Invalid Location !! Please enter a valid location.");
            }

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            MessageBox.Show("Ryde pool is cheaper than Ryde direct. ");
        }
    }
}
